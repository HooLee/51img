#Impala 基础知识
## 1.Impala概述
### 1.1 什么是Impala？
    Impala是用于处理存储在Hadoop集群中的大量数据的MPP（大规模并行处理）SQL查询引擎。
    它是一个用C++和Java编写的开源软件。 与其他Hadoop的SQL引擎相比，它提供了高性能和低延迟。
    换句话说，Impala是性能最高的SQL引擎（提供类似RDBMS的体验），
    它提供了访问存储在Hadoop分布式文件系统中的数据的最快方法。

### 1.2 Impala的功能
    以下是cloudera Impala的功能
    1.Impala可以根据Apache许可证作为开源免费提供。
    2.Impala支持内存中数据处理，即，它访问/分析存储在Hadoop数据节点上的数据，而无需数据移动。
    3.您可以使用Impala使用类SQL查询访问数据。
    4.与其他SQL引擎相比，Impala为HDFS中的数据提供了更快的访问。
    5.使用Impala，您可以将数据存储在存储系统中，如HDFS，Apache HBase和Amazon s3。
    6.您可以将Impala与业务智能工具（如Tableau，Pentaho，Micro策略和缩放数据）集成。
    7.Impala支持各种文件格式，如LZO，序列文件，Avro，RCFile和Parquet。
    8.Impala使用Apache Hive的元数据，ODBC驱动程序和SQL语法。
### 1.3 Impala数据类型
	1,BIGINT,此数据类型存储数值，此数据类型的范围为-9223372036854775808至9223372036854775807.此数据类型在create table和alter table语句中使用。

	2,BOOLEAN,此数据类型只存储true或false值，它用于create table语句的列定义。

	3,CHAR,此数据类型是固定长度的存储，它用空格填充，可以存储最大长度为255。

	4,DECIMAL,此数据类型用于存储十进制值，并在create table和alter table语句中使用。

	5,DOUBLE,此数据类型用于存储正值或负值4.94065645841246544e-324d -1.79769313486231570e + 308范围内的浮点值。

	6,FLOAT,此数据类型用于存储正或负1.40129846432481707e-45 .. 3.40282346638528860e + 38范围内的单精度浮点值数据类型。

	7,INT,此数据类型用于存储4字节整数，范围从-2147483648到2147483647。

	8,SMALLINT,此数据类型用于存储2字节整数，范围为-32768到32767。

	9,STRING,这用于存储字符串值。

	10,TIMESTAMP,此数据类型用于表示时间中的点。

	11,TINYINT,此数据类型用于存储1字节整数值，范围为-128到127。

	12,VARCHAR,此数据类型用于存储可变长度字符，最大长度为65,535。

	13,ARRAY,这是一个复杂的数据类型，它用于存储可变数量的有序元素。

	14,Map,这是一个复杂的数据类型，它用于存储可变数量的键值对。

	15,Struct,这是一种复杂的数据类型，用于表示单个项目的多个字段。
### 1.4 Impala的基本语法
    1.创建数据库
    CREATE DATABASE IF NOT EXISTS database_name;
    例如：CREATE DATABASE IF NOT EXISTS my_database;--将会创建数据库my_database
    可以使用SHOW DATABASES查询给出Impala中的数据库列表，进一步验证
    为了在HDFS文件系统中创建数据库，需要指定要创建数据库的位置。
    CREATE DATABASE IF NOT EXISTS database_name LOCATION hdfs_path;
    2.删除数据库
    Impala的DROP DATABASE语句用于从Impala中删除数据库。 在删除数据库之前，建议从中删除所有表。
    语法如下：
    DROP (DATABASE|SCHEMA) [IF EXISTS] database_name [RESTRICT | 
    CASCADE] [LOCATION hdfs_path];
    例如：
    DROP DATABASE IF EXISTS sample_database;
    一般来说，要删除数据库，您需要手动删除其中的所有表。 如果使用级联，Impala会在删除指定数据库中的表之前删除它。
    例如：
    DROP database sample cascade;
    注意 - 您不能删除Impala中的“当前数据库”。 因此，在删除数据库之前，需要确保将当前上下文设置为除要删除的数据库之外的数据库。
    3.选择数据库
    连接到Impala后，需要从可用的数据库中选择一个。 Impala的USE DATABASE语句用于将当前会话切换到另一个数据库。
    语法如下：
    USE db_name;
    4.创建数据表
    CREATE TABLE语句用于在Impala中的所需数据库中创建新表。 创建基本表涉及命名表并定义其列和每列的数据类型。
    create table IF NOT EXISTS database_name.table_name (
       column1 data_type,
       column2 data_type,
       column3 data_type,
       ………
       columnN data_type
    );
    例如：
    CREATE TABLE IF NOT EXISTS my_db.student(
    	name STRING, 
    	age INT, 
    	contact INT 
    );
    执行以上语句,将会在数据库my_db中创建了一个名为student的表
    5.插入语句
    Insert语句with into子句用于将新记录添加到数据库中的现有表中。
    语法如下：
    insert into table_name (column1, column2, column3,...columnN) values (value1, value2, value3,...valueN);
    您还可以添加值而不指定列名，但是，您需要确保值的顺序与表中的列的顺序相同，如下所示
    Insert into table_name values (value1, value2, value2);
    覆盖表中的数据，语法如下：
    Insert overwrite table_name values (value1, value2, value2);
    我们可以使用覆盖子句覆盖表的记录。 覆盖的记录将从表中永久删除
    6.查询语句
    语法如下：
    SELECT column1, column2, columnN from table_name;
    SELECT * FROM table_name;
    SELECT语句用于从数据库中的一个或多个表中提取数据。
    7.查询表描述信息
    Impala中的describe语句用于提供表的描述。 此语句的结果包含有关表的信息，例如列名称及其数据类型。
    语法如下：
    Describe table_name;
    8.修改表信息
    Impala中的Alter table语句用于对给定表执行更改。 使用此语句，我们可以添加，删除或修改现有表中的列，也可以重命名它们。
    ALTER TABLE重命名现有表的基本语法如下：
    ALTER TABLE [old_db_name.]old_table_name RENAME TO [new_db_name.]new_table_name
    例如：
    ALTER TABLE my_db.customers RENAME TO my_db.users;
    ALTER TABLE向现有表中添加列的基本语法如下：
    ALTER TABLE name ADD COLUMNS (col_spec[, col_spec ...])
    例如：
    ALTER TABLE users ADD COLUMNS (account_no BIGINT, phone_no BIGINT);
    现有表中ALTER TABLE到DROP COLUMN的基本语法如下：
    ALTER TABLE name DROP [COLUMN] column_name
    例如：
    ALTER TABLE users DROP account_no;
    ALTER TABLE更改现有表中的列的名称和数据类型的基本语法如下：
    ALTER TABLE name CHANGE column_name new_name new_type
    例如：
    ALTER TABLE users CHANGE phone_no e_mail string;
    这里我们将列phone_no的名称更改为电子邮件，将其数据类型更改为字符串
    9.删除表信息
    Impala drop table语句用于删除Impala中的现有表。 此语句还会删除内部表的底层HDFS文件
    注意 - 使用此命令时必须小心，因为删除表后，表中可用的所有信息也将永远丢失。
    语法如下：
    DROP table database_name.table_name;
    例如：
    drop table if exists my_db.student;
    如果尝试删除不存在IF EXISTS子句的表，将会生成错误。
    10.截断表：
    Impala的Truncate Table语句用于从现有表中删除所有记录。
    语法如下：
    truncate table_name;
    例如：
    truncate customers;
    我们删除名为customers的表的所有记录。
    11.显示表：
    Impala中的show tables语句用于获取当前数据库中所有现有表的列表
    12.创建视图：
    视图仅仅是存储在数据库中具有关联名称的Impala查询语言的语句。 它是以预定义的SQL查询形式的表的组合。
    视图可以包含表的所有行或选定的行。 可以从一个或多个表创建视图。 视图允许用户 - 
    	①.以用户或用户类发现自然或直观的方式结构化数据。
    	②.限制对数据的访问，以便用户可以看到和（有时）完全修改他们需要的内容，而不再更改。
    	③.汇总可用于生成报告的各种表中的数据。
    您可以使用Impala的Create View语句创建视图。
    语法如下：
    Create View IF NOT EXISTS view_name as Select statement
    IF NOT EXISTS是一个可选的子句。 如果使用此子句，则只有在指定数据库中没有具有相同名称的现有表时，才会创建具有给定名称的表。
    13.更改视图
    Impala的Alter View语句用于更改视图。 使用此语句，您可以更改视图的名称，更改数据库以及与其关联的查询。
    由于视图是一个逻辑结构，因此没有物理数据会受到alter view查询的影响。
    语法如下：
    ALTER VIEW database_name.view_name as Select statement
    14.删除视图：
    Impala的Drop View查询用于删除现有视图。 由于视图是一个逻辑结构，因此没有物理数据将受到视图查询的影响。
    语法如下：
    DROP VIEW database_name.view_name;
    例如：
    Drop view customers_view;
    15.ORDER BY子句
    Impala ORDER BY子句用于根据一个或多个列以升序或降序对数据进行排序。 默认情况下，一些数据库按升序对查询结果进行排序。
    语法如下：
    select col1,col2...colN from table_name ORDER BY col_name [ASC|DESC] [NULLS FIRST|NULLS LAST]
    可以使用关键字ASC或DESC分别按升序或降序排列表中的数据。
    以同样的方式，如果我们使用NULLS FIRST，表中的所有空值都排列在顶行; 如果我们使用NULLS LAST，包含空值的行将最后排列。
    16.GROUP BY子句
    Impala GROUP BY子句与SELECT语句协作使用，以将相同的数据排列到组中。
    语法如下：
    select data from table_name Group BY col_name;
    17.having子句
    一般来说，Having子句与group by子句一起使用; 它将条件放置在由GROUP BY子句创建的组上
    例如：
    select max(salary) from customers group by age having max(salary) > 20000;
    此查询最初按年龄对表进行分组，并选择每个组的最大工资，并显示大于20000的工资
    18.limit子句
    Impala中的limit子句用于将结果集的行数限制为所需的数，即查询的结果集不包含超过指定限制的记录。
    例如：
    select * from customers order by id limit 4;
    将输出的记录数限制为4
    19.offset子句
    一般来说，select查询的resultset中的行从0开始。使用offset子句，我们可以决定从哪里考虑输出。
    例如：
    select * from customers order by id limit 4 offset 5;
    从具有偏移5的行开始从客户表获取四个记录，
    20.Union子句
    您可以使用Impala的Union子句组合两个查询的结果。
    语法如下：
    query1 union query2;
    例如：
    select * from customers order by id limit 3 union select * from employee order by id limit 3;
    21.with子句
    如果查询太复杂，我们可以为复杂部分定义别名，并使用Impala的with子句将它们包含在查询中。
    语法如下：
    with x as (select 1), y as (select 2) (select * from x union y);
    例如：
    with t1 as (select * from customers where age>25), t2 as (select * from employee where age>25) 
       (select * from t1 union select * from t2)
    22.DISTINCT运算符
    Impala中的distinct运算符用于通过删除重复值来获取唯一值。
    语法如下：
    select distinct columns… from table_name;
    例如：
    select distinct name, age, address from customers;
    