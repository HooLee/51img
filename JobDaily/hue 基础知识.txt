1. Hue是什么？
    Hue是一个开源的Apache Hadoop UI系统，最早是由Cloudera Desktop演化而来，
    由Cloudera贡献给开源社区，它是基于Python Web框架Django实现的。
2. Hue能干什么？
    2.1 访问HDFS和文件浏览
    2.2 通过web调试和开发hive以及数据结果展示
    2.3 查询solr和结果展示，报表生成
    2.4 通过web调试和开发impala交互式SQL Query
    2.5 spark调试和开发
    2.6 Pig开发和调试
    2.7 Oozie任务的开发，监控，和工作流协调调度
    2.8 Hbase数据查询和修改，数据展示
    2.9 Hive的元数据（metastore）查询
    2.10 MapReduce任务进度查看，日志追踪
    2.11 创建和提交MapReduce，Streaming，Java job任务
    2.12 Sqoop2的开发和调试
    2.13 Zookeeper的浏览和编辑
    2.14 数据库（MySQL，PostGres，SQlite，Oracle）的查询和展示
3. Hue的优势是什么?
    通过使用Hue我们可以在浏览器端的Web控制台上与hadoop集群进行交互来分析处理数据
4. Hue的整体架构图
    见img文件夹下的 hue整体架构图.png
5.